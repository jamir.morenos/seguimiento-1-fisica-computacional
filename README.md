# Seguimiento 1 Física Computacional
# Seguimiento 1 Física Computacional

Problema : 

Deben simular el comportamiento de dos partículas cargadas moviéndose en un campo magnético constante con las siguientes condiciones:
1. Aceleración inicial nula.
2. Velocidad inicial no nula.
3. Las partículas inician acercándose.
4. Campo magnético no nulo.

Son de libre elección:
1. La masa de las partículas.
2. La carga de las partículas.
3. Las posiciones iniciales de las partículas.

Para este ejercicio considere dos fuerzas, la de Coulomb debida a la carga de las dos partículas y la de Lorentz para modelar la interacción de las partículas con el campo magnético. Utilice las ecuaciones para un movimiento uniformemente acelerado para simular la dinámica de las partículas en cada paso de tiempo:

a=constante
v=v0+a*t
x=x0+v0*t+0.5*a*t**2

Descripcion del codigo: el siguiente codigo realiza la solucion, es decir la descripcion cinematica i,e de las variables cinematicas del problema de dos particulas interactuantes mediante fuerza de coulomb (es decir, cargadas) bajo un campo magnetico uniforme en todo el espacio y cuya aceleracion incial es 0.

Es por esto que se decidio crear dos clases una para editar las particulas y la otra para resolver el problema dinámica, en esta clase tambien este los metodos para graficar la posiciones, velocidades y aceleracion en 3 dimensiones.

La importancia del problema es que puede servir como ayuda didactica, para apoyar los analisis electrodinamicos, como por ejemplo el ciclotron, aunque claro esta la forma como se soluciono la dinamica de dos particulas es simplista y en realidad se deben considerar más ajustes para una dinamica mas afin a la realidad.






proyecto desarollado y mantenido por: Jamir Moreno Salazar, Daniel Valle Jaramillo, Juan Sebastian Sanchez.
Contacto: jamir.morenos@udea.edu.co, daniel.vallej@udea.edu.co, jsebastian.sanchez@udea.edu.co

