#librerias:
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import plotly.graph_objects as go


# Descripcion del codigo: el siguiente codigo realiza la solucion, es decir la descripcion cinematica i,e de las variables cinematicas
# del problema de dos particulas interactuantes mediante fuerza de coulomb (es decir, cargadas) bajo un campo magnetico uniforme en todo el espacio y
# cuya aceleracion incial es 0.

# En la siguiente clase, se plantea que el usuario pueda crear tanta particulas el quiera (es decir, particulas con diferentes parametros, de las cuales el
# quiera conocer su descripcion cinematica bajo el problema previamente mencionado).
class Particulas:
  contador_particulas = 0                                         # Contador estático para enumerar las partículas
  Caracteristica_por_defecto_posicion_incial = np.array([1,1,1])  # atributo-caracteristica-propiedad por defecto que tendran los objetos de la clase particula (i,e las particulas) 
  def __init__(self, carga_partc_elegida, masa_partc_elegida, posicion_incial_partc_elegida, velocidad_inicial_partc_elegida):
    self.id_particula = Particulas.contador_particulas            # Asigna el ID único a la partícula
    Particulas.contador_particulas += 1                           # Incrementa el contador cada vez que se crea una nueva partícula
    self.carga_a_inicializar = carga_partc_elegida                # carga a inicializar es lo mismo que carga a determinar o escoger...
    self.masa_a_inicializar = masa_partc_elegida
    self.posicion_incial_a_inicializar = np.array(posicion_incial_partc_elegida)         # pues [x0,y0,z0]
    self.velocidad_incial_a_inicializar = np.array(velocidad_inicial_partc_elegida)      # pues [vx0,vy0,vz0]


# Una vez el usuario cree al menos dos particulas, podra utilizar la siguiente clase.
class Dinamica_de_las_particulas:
    #._.

    def __init__(self, particula1, particula2, B, t):
        """" Esta clase define el estado y movimiento de las partículas, con los siguientes parámetros:
        un objeto partícula, otro objeto partícula, valor del campo magnético, más el intervalo temporal
        en el cual se quiera analizar o resolver dicho problema (y en última instancia, graficarlo).
        """
        self.B = np.array(B)
        self.t = t
        self.masa1 = particula1.masa_a_inicializar
        self.masa2 = particula2.masa_a_inicializar
        self.carga1 = particula1.carga_a_inicializar
        self.carga2 = particula2.carga_a_inicializar
        self.posicion01 = particula1.posicion_incial_a_inicializar
        self.posicion02 = particula2.posicion_incial_a_inicializar
        self.velocidad01 = particula1.velocidad_incial_a_inicializar
        self.velocidad02 = particula2.velocidad_incial_a_inicializar
        self.id_particula1 = particula1.id_particula
        self.id_particula2 = particula2.id_particula
        self.fuerza_coulomb1 = [0,0,0]
        self.fuerza_coulomb2 = [0,0,0]
        self.fuerza_lorentz1 = [0,0,0]
        self.fuerza_lorentz2 = [0,0,0]



    def coulombForce(self):
        #Los valores estan definidos en unidades naturales, asi K=1, B=1,
        #las cargas y masas de las particulas son 1 o +- 1 en el caso de las cargas
        K  = 1                                                                             
        try:
            r0 = np.sum((self.posicion01 - self.posicion02)**2) **(1/2)                                
            if r0==0:
                raise ZeroDivisionError("Las partículas están en la misma posición.")
        except ZeroDivisionError as e:
          self.posicion01 = Particulas.Caracteristica_por_defecto_posicion_incial
          self.posicion02 = Particulas.Caracteristica_por_defecto_posicion_incial + 1
          r0 = np.sum((self.posicion01 - self.posicion02)**2) **(1/2)                                 
          print(e, "se tomaran las posiciones respectivamente: partc", self.id_particula1 ,self.posicion01, "partc", self.id_particula2, self.posicion02)

        fuerza_coulomb_x = K*(self.carga1 * self.carga2)  /  r0**2 * (self.posicion01[0]-self.posicion02[0])/r0
        fuerza_coulomb_y = K*(self.carga1 * self.carga2)  /  r0**2 * (self.posicion01[1]-self.posicion02[1])/r0
        fuerza_coulomb_z = K*(self.carga1 * self.carga2)  /  r0**2 * (self.posicion01[2]-self.posicion02[2])/r0

        self.fuerza_coulomb1 = np.array([fuerza_coulomb_x, fuerza_coulomb_y, fuerza_coulomb_z])
        self.fuerza_coulomb2 = -np.array([fuerza_coulomb_x, fuerza_coulomb_y, fuerza_coulomb_z])

    def magnetForce(self):
        fuerza_lorentz_x1 = self.carga1 * (self.velocidad01[1]*self.B[2] - self.velocidad01[2]*self.B[1])
        fuerza_lorentz_x2 = self.carga2 * (self.velocidad02[1]*self.B[2] - self.velocidad02[2]*self.B[1])

        fuerza_lorentz_y1 = self.carga1 * (self.velocidad01[2]*self.B[0] - self.velocidad01[0]*self.B[2])
        fuerza_lorentz_y2 = self.carga2 * (self.velocidad02[2]*self.B[0] - self.velocidad02[0]*self.B[2])

        fuerza_lorentz_z1 = self.carga1 * (self.velocidad01[0]*self.B[1] - self.velocidad01[1]*self.B[0])
        fuerza_lorentz_z2 = self.carga2 * (self.velocidad02[0]*self.B[1] - self.velocidad02[1]*self.B[0])

        self.fuerza_lorentz1 = np.array([fuerza_lorentz_x1,fuerza_lorentz_y1,fuerza_lorentz_z1])
        self.fuerza_lorentz2 = np.array([fuerza_lorentz_x2,fuerza_lorentz_y2,fuerza_lorentz_z2])

    def movEq(self):
        self.coulombForce()
        self.magnetForce()
        guardando_posicion_inicial_original_partc1    = self.posicion01
        guardando_velocidad_inicial_original_partc1   = self.velocidad01
        guardando_aceleracion_inicial_original_partc1 = ( self.fuerza_coulomb1 + self.fuerza_lorentz1 )/self.masa1

        guardando_posicion_inicial_original_partc2    = self.posicion02
        guardando_velocidad_inicial_original_partc2   = self.velocidad02
        guardando_aceleracion_inicial_original_partc2 = ( self.fuerza_coulomb2 + self.fuerza_lorentz2 )/self.masa2

        posicion1L = []
        posicion2L = []

        velocidad1L = []
        velocidad2L = []

        aceleracion1L = []
        aceleracion2L = []

        for j in self.t:

            aceleracion1L.append( (self.fuerza_coulomb1 + self.fuerza_lorentz1 )/self.masa1 )
            aceleracion2L.append( (self.fuerza_coulomb2 + self.fuerza_lorentz2 )/self.masa2 )

            posicion1 = self.posicion01 + self.velocidad01*j + 0.5*j**2*( self.fuerza_coulomb1+self.fuerza_lorentz1 )/self.masa1
            posicion2 = self.posicion02 + self.velocidad02*j + 0.5*j**2*( self.fuerza_coulomb2+self.fuerza_lorentz2 )/self.masa2
            posicion1L.append(posicion1)
            posicion2L.append(posicion2)

            velocidad1 = self.velocidad01 + j*( self.fuerza_coulomb1+self.fuerza_lorentz1 )/self.masa1
            velocidad2 = self.velocidad02 + j*( self.fuerza_coulomb2+self.fuerza_lorentz2 )/self.masa2
            velocidad1L.append(velocidad1)
            velocidad2L.append(velocidad2)

            self.posicion01 = posicion1
            self.posicion02 = posicion2

            self.velocidad01 = velocidad1
            self.velocidad02 = velocidad2

            self.coulombForce()
            self.magnetForce()

        self.posicion01  = guardando_posicion_inicial_original_partc1
        self.velocidad01 = guardando_velocidad_inicial_original_partc1
        c = guardando_aceleracion_inicial_original_partc1

        self.posicion02  = guardando_posicion_inicial_original_partc2
        self.velocidad02 = guardando_velocidad_inicial_original_partc2
        g = guardando_aceleracion_inicial_original_partc2

        self.positionarray = posicion1L #Jamir; con el fin de acceder a el objeto "posicion1L"
        return c, g, posicion1L, velocidad1L, posicion2L, velocidad2L, aceleracion1L, aceleracion2L

    def graficado_mov_3d(self):
    # Ejecutar movEq para obtener los datos necesarios
        c, g, posicion1L, velocidad1L, posicion2L, velocidad2L, aceleracion1L, aceleracion2L = self.movEq()
        posicion1L = np.array(posicion1L)
        posicion2L = np.array(posicion2L)

        velocidad1L= np.array(velocidad1L)
        velocidad2L= np.array(velocidad2L)

        aceleracion1L= np.array(aceleracion1L)
        aceleracion2L= np.array(aceleracion2L)

    # Crear la figura para el gráfico en 3D
        fig1 = go.Figure()
        fig2 = go.Figure()
        fig3 = go.Figure()

    # Graficar las trayectorias de las partículas en 3D
        fig1.add_trace(go.Scatter3d(
            x=posicion1L[:, 0], y=posicion1L[:, 1], z=posicion1L[:, 2],
            mode='lines', name=f'Partícula {self.id_particula1}',
            marker=dict(size=4, color='blue'),
            line=dict(color='blue', width=2)
        ))

        fig1.add_trace(go.Scatter3d(
            x=posicion2L[:, 0], y=posicion2L[:, 1], z=posicion2L[:, 2],
            mode='lines', name=f'Partícula {self.id_particula2}',
            marker=dict(size=4, color='red'),
            line=dict(color='red', width=2)
        ))

    # Marcar el origen con un punto rojo y grande para referencia
        fig1.add_trace(go.Scatter3d(
            x=[0], y=[0], z=[0],
            mode='markers', name='Origen',
            marker=dict(size=10, color='black')
        ))

    # Ajustar el diseño y las etiquetas
        fig1.update_layout(
            scene=dict(
                xaxis_title='Posición X',
                yaxis_title='Posición Y',
                zaxis_title='Posición Z',
            ),
            title='Trayectoria de las Partículas en 3D con Condiciones Iniciales'
        )

    # Mostrar el gráfico interactivo
        fig1.show()

        fig2.add_trace(go.Scatter3d(
            x=velocidad1L[:, 0], y=velocidad1L[:, 1], z=velocidad1L[:, 2],
            mode='lines', name=f'Partícula {self.id_particula1}',
            marker=dict(size=4, color='blue'),
            line=dict(color='blue', width=2)
        ))

        fig2.add_trace(go.Scatter3d(
            x=velocidad2L[:, 0], y=velocidad2L[:, 1], z=velocidad2L[:, 2],
            mode='lines', name=f'Partícula {self.id_particula2}',
            marker=dict(size=4, color='red'),
            line=dict(color='red', width=2)
        ))

    # Marcar el origen con un punto rojo y grande para referencia
        fig2.add_trace(go.Scatter3d(
            x=[0], y=[0], z=[0],
            mode='markers', name='Origen',
            marker=dict(size=10, color='black')
        ))

    # Ajustar el diseño y las etiquetas
        fig2.update_layout(
            scene=dict(
                xaxis_title='Velocidad X',
                yaxis_title='Velocidad Y',
                zaxis_title='Velocidad Z',
            ),
            title='Velocidad de las Partículas en 3D con Condiciones Iniciales'
        )

    # Mostrar el gráfico interactivo
        fig2.show()

        fig3.add_trace(go.Scatter3d(
            x=aceleracion1L[:, 0], y=aceleracion1L[:, 1], z=aceleracion1L[:, 2],
            mode='lines', name=f'Partícula {self.id_particula1}',
            marker=dict(size=4, color='blue'),
            line=dict(color='blue', width=2)
        ))

        fig3.add_trace(go.Scatter3d(
            x=aceleracion2L[:, 0], y=aceleracion2L[:, 1], z=aceleracion2L[:, 2],
            mode='lines', name=f'Partícula {self.id_particula2}',
            marker=dict(size=4, color='red'),
            line=dict(color='red', width=2)
        ))

    # Marcar el origen con un punto rojo y grande para referencia
        fig3.add_trace(go.Scatter3d(
            x=[0], y=[0], z=[0],
            mode='markers', name='Origen',
            marker=dict(size=10, color='black')
        ))

    # Ajustar el diseño y las etiquetas
        fig3.update_layout(
            scene=dict(
                xaxis_title='Aceleracion X',
                yaxis_title='Aceleracion Y',
                zaxis_title='Aceleracion Z',
            ),
            title='Aceleracion de las Partículas en 3D con Condiciones Iniciales'
        )

    # Mostrar el gráfico interactivo
        fig3.show()


###---------------------------##########################################
        
# Creación de dos partículas para prueba
particula1 = Particulas(1, 1, [-1, 0, 0], [0, -1, 0])
particula2 = Particulas(-1, 1, [1, 0, 1], [0, 1, 0])

# Creación de la instancia de Dinamica_de_las_particulas con las partículas creadas
simulacion = Dinamica_de_las_particulas(particula1, particula2, [0, 0, 1], np.linspace(0, 0.5, 100))

# Ejecución del método de graficado
simulacion.graficado_mov_3d()